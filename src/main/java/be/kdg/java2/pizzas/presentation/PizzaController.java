package be.kdg.java2.pizzas.presentation;

import be.kdg.java2.pizzas.domain.Ingredient;
import be.kdg.java2.pizzas.domain.PizzaOrder;
import be.kdg.java2.pizzas.presentation.dto.PizzaOrderDTO;
import be.kdg.java2.pizzas.repository.IngredientsListRepository;
import be.kdg.java2.pizzas.repository.PizzaOrderListRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/pizzas")
public class PizzaController {
    private PizzaOrderListRepository pizzaListRepository;
    private IngredientsListRepository ingredientsListRepository;

    public PizzaController(PizzaOrderListRepository pizzaListRepository, IngredientsListRepository ingredientsListRepository) {
        this.pizzaListRepository = pizzaListRepository;
        this.ingredientsListRepository = ingredientsListRepository;
    }

    @GetMapping
    public String showHomepage(){
        return "index";
    }

    @GetMapping("/orders")
    public String showOrdersOverview(Model model){
        model.addAttribute("orders", pizzaListRepository.read());
        return "orders";
    }

    @GetMapping("/order")
    public String showOrderForm(Model model){
        model.addAttribute("ingredients", ingredientsListRepository.read());
        return "placeorder";
    }

    @PostMapping("/order")
    public String processOrderForm(PizzaOrderDTO orderDTO){
        List<Ingredient> ingredients = new ArrayList<>();
        orderDTO.getIngredientIds().forEach((id)->ingredients.add(ingredientsListRepository.findById(id)));
        PizzaOrder pizzaOrder = new PizzaOrder(orderDTO.getName(),ingredients);
        pizzaListRepository.create(pizzaOrder);
        return "redirect:/pizzas/orders";
    }
}
