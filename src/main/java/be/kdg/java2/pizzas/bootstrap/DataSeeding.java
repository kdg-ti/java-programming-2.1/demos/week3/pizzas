package be.kdg.java2.pizzas.bootstrap;

import be.kdg.java2.pizzas.domain.Ingredient;
import be.kdg.java2.pizzas.domain.PizzaOrder;
import be.kdg.java2.pizzas.repository.EntityRepository;
import be.kdg.java2.pizzas.repository.IngredientsListRepository;
import be.kdg.java2.pizzas.repository.PizzaOrderListRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class DataSeeding implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(DataSeeding.class);
    private PizzaOrderListRepository pizzaRepository;
    private IngredientsListRepository ingredientRepository;

    public DataSeeding(PizzaOrderListRepository pizzaRepository, IngredientsListRepository ingredientRepository) {
        this.pizzaRepository = pizzaRepository;
        this.ingredientRepository = ingredientRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        log.debug("Seeding the repositories...");
        Ingredient pineapple = new Ingredient("pineapple");
        Ingredient tomatoSauce = new Ingredient("tomato sauce");
        Ingredient cheese = new Ingredient("cheese");
        Ingredient pepperoni = new Ingredient("pepperoni");
        Ingredient onion = new Ingredient("onion");
        Ingredient olives = new Ingredient("olives");
        Ingredient spicyPeppers = new Ingredient("spicy peppers");
        Ingredient mushrooms = new Ingredient("mushrooms");
        ingredientRepository.create(pineapple);
        ingredientRepository.create(tomatoSauce);
        ingredientRepository.create(cheese);
        ingredientRepository.create(pepperoni);
        ingredientRepository.create(onion);
        ingredientRepository.create(olives);
        ingredientRepository.create(spicyPeppers);
        ingredientRepository.create(mushrooms);
        pizzaRepository.create(new PizzaOrder("Johnny", Arrays.asList(tomatoSauce, cheese, pineapple)));
        pizzaRepository.create(new PizzaOrder("Antoinette", Arrays.asList(tomatoSauce, cheese)));
        pizzaRepository.create(new PizzaOrder("Maria", Arrays.asList(olives, cheese, pepperoni)));
    }
}
